## Test environments

* local OS X install, R 3.6.1
* Continuous Integration
  * Ubuntu Trusty 14.04 on travis-ci (devel and release)
  * macOS on travis-ci (devel and release)
  * Windows Server 2012 on appveyor (devel and release)
* Rhub
  * Debian Linux, R-devel & R-devel, GCC ASAN/UBSAN
  * Fedora Linux, R-devel & R-devel, clang, gfortran

## R CMD check results

0 errors | 0 warnings | 1 note

* This is a new release.

## Addressed comments

* Omitted the redundant "R" from the title.

* Explained all acronyms in the description text.

* Used single quotes to write package names, software names and API names
in title and description. e.g: 'uklr' & 'SPARQL'

* Added 'value' entry to to all .Rd files.
