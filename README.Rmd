---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```
# uklr <img src='man/figures/logo.png' align="right" height="138.5" />

<!-- badges: start -->
[![CRAN status](https://www.r-pkg.org/badges/version/uklr)](https://CRAN.R-project.org/package=uklr)
[![Lifecycle: maturing](https://img.shields.io/badge/lifecycle-maturing-blue.svg)](https://www.tidyverse.org/lifecycle/#maturing)
[![Travis build status](https://travis-ci.org/kvasilopoulos/uklr.svg?branch=master)](https://travis-ci.org/kvasilopoulos/uklr)
[![AppVeyor build status](https://ci.appveyor.com/api/projects/status/github/kvasilopoulos/uklr?branch=master&svg=true)](https://ci.appveyor.com/project/kvasilopoulos/uklr)
[![Codecov test coverage](https://codecov.io/gh/kvasilopoulos/uklr/branch/master/graph/badge.svg)](https://codecov.io/gh/kvasilopoulos/uklr?branch=master)
<!-- badges: end -->

The goal of {uklr} is to access data from HM Land Registry Open Data <http://landregistry.data.gov.uk/> through SPARQL queries. {uklr} supports the UK HPI, Transaction Data and Price Paid Data.

## Installation

You can install the development version from [GitHub](https://github.com/) with:

``` r
# install.packages("devtools")
devtools::install_github("kvasilopoulos/uklr")
```
## Example

```{r eval = TRUE}

library(uklr)

ukhp_get(region = "newport", start_date = "2019-01-01")

ukppd_get(postcode = "PL6 8RU", start_date = "2015-01-01")

uktrans_get(item = "totalApplicationCountByRegion", region = "East Anglia")

```

